import React, { Component } from 'react';
import SwedishCrossword from '../../components/Crossword/SwedishCrossword/SwedishCrossword';
import ICrosswordQuestion from '../../components/Crossword/ICrosswordQuestion';
import { QuestionCellDirection } from '../../components/Crossword/cells/QuestionCell/QuestionCell';

const cw : Array<ICrosswordQuestion> = [
  {
    x: 1,
    y: 0,
    questionShort: 'Ehem. dt. Kaiser',
    questionLong: 'Ehemaliger deutscher Kaiser',
    direction: QuestionCellDirection.VERTICAL,
    answer: 'Wilhelm'
  },
  {
    x: 0,
    y: 2,
    questionShort: 'And. Wort f. "Eiland"',
    questionLong: 'Anderes Wort für "Eiland"',
    direction: QuestionCellDirection.HORIZONTAL,
    answer: 'Insel'
  },
  {
    x: 6,
    y: 8,
    questionShort: 'Romanes eunt domus',
    questionLong: 'Lorem Ipsum dolor',
    direction: QuestionCellDirection.HORIZONTAL,
    answer: 'Bla'
  }
];

class App extends Component {
  render() {
    return (
      <div style={{ width: '40vw', margin: '2em auto' }}>
        <SwedishCrossword showEmpty
          columns={10}
          rows={10}
          questions={cw}
        />
      </div>
    );
  }
}

export default App;
