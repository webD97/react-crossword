import React, { useEffect, useRef } from 'react';

const cellStyle : React.CSSProperties = {
    backgroundColor: 'white',
    width: '100%',
    paddingTop: '100%',
    height: '0',
    position: 'relative',
    color: 'black',
};

const contentStyle : React.CSSProperties = {
    position: 'absolute',
    top: 0,
    left: 0,
    textAlign: 'center',
    display: 'block',
    width: 'calc(100% - 2px)',
    height: 'calc(100% - 2px)',
    border: 'none',
    textTransform: 'uppercase'
}

const InputCell = (props: any) => {
    const inputNode = useRef<HTMLInputElement>(null);

    useEffect(() => {
        const node = inputNode.current;
        if (node) {
            const fontSize = (node.clientHeight * 0.6) + 'px';
            node.style.fontSize = fontSize;
            node.style.lineHeight = fontSize;
        }
    }, []);

    return (
        <div style={cellStyle}>
            <input style={contentStyle} type="text" maxLength={1} ref={inputNode} />
        </div>
    );
}

export default InputCell;
