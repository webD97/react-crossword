import React from 'react';

import styles from './QuestionCell.module.css';

export enum QuestionCellDirection {
    HORIZONTAL = 'HORIZONTAL',
    VERTICAL = 'VERTICAL'
}

interface QuestionCellProps {
    question: string,
    direction: QuestionCellDirection
}

const QuestionCell = (props: QuestionCellProps) => {
    const classNames = [styles.questionCell];

    if (props.direction === QuestionCellDirection.HORIZONTAL) {
        classNames.push(styles.right);
    }
    else if (props.direction === QuestionCellDirection.VERTICAL) {
        classNames.push(styles.down);
    }

    return (
        <div className={classNames.join(' ')}>
            <div>
                <span>{props.question}</span>
            </div>
        </div>
    );
}

export default QuestionCell;
