import React from 'react';

import styles from './EmptyCell.module.css';

const EmptyCell = () => {
    return (
        <div className={styles.emptyCell}>&nbsp;</div>
    );
}

export default EmptyCell;
