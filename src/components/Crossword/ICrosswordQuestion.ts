import { QuestionCellDirection } from "./cells/QuestionCell/QuestionCell";

export default interface ICrosswordQuestion {
    x: number,
    y: number,
    questionShort: string,
    questionLong?: string,
    direction: QuestionCellDirection,
    answer: string
};