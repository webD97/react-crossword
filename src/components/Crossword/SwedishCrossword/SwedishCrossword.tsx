import React from 'react';
import EmptyCell from '../cells/EmptyCell/EmptyCell';
import QuestionCell, { QuestionCellDirection } from '../cells/QuestionCell/QuestionCell';
import InputCell from '../cells/InputCell/InputCell';

import styles from './SwedishCrossword.module.css';
import ICrosswordQuestion from '../ICrosswordQuestion';

const makeKey = (x: number, y: number) => x + '_' + y;

interface SwedishCrosswordProps {
    columns: number,
    rows: number,
    questions: Array<ICrosswordQuestion>,
    showEmpty?: boolean
}

const SwedishCrossword = (props: SwedishCrosswordProps) => {
    const cwDimensions = { x: 0, y: 0};
    const cwMap = new Map<string, React.ComponentElement<any, any>>();

    props.questions.forEach(question => {
        const questionPos = { x: question.x, y: question.y };
        const answerLength = question.answer.length;

        // Update dimensions
        if (question.direction === QuestionCellDirection.HORIZONTAL) {
            const outermostX = questionPos.x + question.answer.length + 1;
            const outermostY = questionPos.y + 1;
            cwDimensions.x = outermostX > cwDimensions.x ? outermostX : cwDimensions.x;
            cwDimensions.y = outermostY > cwDimensions.y ? outermostY : cwDimensions.y;
        }
        else if (question.direction === QuestionCellDirection.VERTICAL) {
            const outermostX = questionPos.x + 1;
            const outermostY = questionPos.y + question.answer.length + 1;
            cwDimensions.x = outermostX > cwDimensions.x ? outermostX : cwDimensions.x;
            cwDimensions.y = outermostY > cwDimensions.y ? outermostY : cwDimensions.y;
        }

        // Add question itself
        cwMap.set(makeKey(questionPos.x, questionPos.y), (
            <div
                key={makeKey(questionPos.x, questionPos.y)}
                title={question.questionLong ? question.questionLong : question.questionShort}
                style={{ gridColumn: `${questionPos.x + 1}/${questionPos.x + 1}`, gridRow: `${questionPos.y + 1}/${questionPos.y + 1}` }}
            >
                <QuestionCell question={question.questionShort} direction={question.direction} />
            </div>
        ));
        
        
        // Add answer fields
        const makeInput = (pos: {x: number, y: number}) => (
            <div
                key={makeKey(pos.x, pos.y)}
                style={{ gridColumn: `${pos.x + 1}/${pos.x + 1}`, gridRow: `${pos.y + 1}/${pos.y + 1}` }}
            >
                <InputCell />
            </div>
        );

        if (question.direction === QuestionCellDirection.HORIZONTAL) {
            for (let i = 0; i < answerLength; i++) {
                const answerPos = { x: question.x + i + 1, y: question.y };
                cwMap.set(makeKey(answerPos.x, answerPos.y), makeInput(answerPos));
            }
        }
        else if (question.direction === QuestionCellDirection.VERTICAL) {
            for (let i = 0; i < answerLength; i++) {
                const answerPos = { x: question.x, y: question.y + i + 1 };
                cwMap.set(makeKey(answerPos.x, answerPos.y), makeInput(answerPos));
            }
        }
    });

    // Fill the rest with empty cells
    if (props.showEmpty) {
        for (let x = 0; x < cwDimensions.x; x++) {
            for (let y = 0; y < cwDimensions.y; y++) {
                const position = {x: x, y: y};
                const cell = cwMap.get(makeKey(x, y));
    
                if (!cell) {
                    cwMap.set(makeKey(x, y), (
                        <div
                            key={makeKey(x, y)}
                            style={{ gridColumn: `${position.x + 1}/${position.x + 1}`, gridRow: `${position.y + 1}/${position.y + 1}` }}
                        >
                            <EmptyCell />
                        </div>
                    ));
                }
            }
        }
    }

    return (
        <div className={styles.swedishCrossword}>
            {
                Array.from(cwMap.values())
            }
        </div>
    );
}

export default SwedishCrossword;
